//
// Created by ice_tea on 13.02.19.
//

#include "KMP.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>

const int BLOCK_SIZE = 1000000;

std::string read_block(std::ifstream& in) {
    std::string result;

    for (int i = 0; i < BLOCK_SIZE && !in.eof(); ++i) {
        char c;

        in.get(c);

        result += c;
    }

    return result;
}

void print_vector(const std::vector<int>& ar) {
    for (auto el : ar)
        std::cout << el << " ";
}

std::string& toUpperCase(std::string& str) {
    for (auto& el : str) {
        _toupper(el);
    }

    return str;
}

std::vector<int> find_sub_str(const std::string& text, const std::string& str) {

    std::vector<int> result;

    for (int i = 0; i <= text.size() - str.size(); ++i) {

        int j;
        for (j = 0; j < str.size(); ++j) {
            if (text[i + j] != str[j])
                break;
        }

        if (j == str.size())
            result.push_back(i);
    }

    return result;
}

int main() {

    for (int i = 0; i < 1; ++i) {

        std::string sub_str;
        getline(std::cin, sub_str);
        std::transform(sub_str.begin(), sub_str.end(), sub_str.begin(), ::toupper);

        std::ifstream in("data/genome.txt");
        bool is_open = in.is_open();
        std::string last_block;

        std::vector<int> substrs;

        std::chrono::duration<double> elapsed_seconds{};

        while (!in.eof()) {
            std::string text_block = read_block(in);
            std::string full_block = last_block + text_block;

            std::transform(full_block.begin(), full_block.end(), full_block.begin(), ::toupper);

            auto start = std::chrono::system_clock::now();
            std::vector<int> result = kmp(full_block, sub_str);
            auto end = std::chrono::system_clock::now();

            elapsed_seconds += end - start;

            substrs.insert(substrs.begin(), result.begin(), result.end());
        }

        in.close();

        std::ofstream out;

        out.open("../results/genome_kmp_result.txt", std::ios_base::app);

        out << substrs.size() << " " << elapsed_seconds.count() << " " << sub_str << "\n";

        out.close();

    }


}
