//
// Created by ice_tea on 13.02.19.
//

#include <vector>
#include <string>
#include <algorithm>

std::vector<int> z_function(const std::string& str) {
    std::vector<int> result(str.size());

    for (int i = 1, l = 0, r = 0; i < str.size(); ++i) {
        if (i <= r)
            result[i] = std::min(r - i + 1, result[i - l]);
        while (result[i] + i < str.size() && str[result[i]] == str[result[i] + i])
            ++result[i];
        if (i + result[i] - 1 > r) {
            r = i + result[i] - 1;
            l = i;
        }
    }

    return result;
}

std::vector<int> kmp(const std::string &text, const std::string &str) {
    std::string u_str = str + "$" + text;

    std::vector<int> z_func = z_function(u_str);
    std::vector<int> result;
    for (int i = (int)str.size() + 1; i < u_str.size(); ++i) {
        if (z_func[i] == str.size())
            result.push_back(i - (int)str.size() - 1);
    }

    return result;
}

bool is_cycling_shift(const std::string &f_str, const std::string &s_str) {

    if (f_str.size() != s_str.size())
        return false;

    std::string u_str = f_str + f_str;

    std::vector<int> results = kmp(u_str, s_str);
    return !results.empty();
}

bool is_equal_with_cycling(const std::string& cycling_str, const std::string& str) {
    if (cycling_str.size() > str.size())
        return !kmp(cycling_str, str).empty();

    std::string u_str = cycling_str + "$" + str;
    std::vector<int> z_func = z_function(u_str);

    int f = -1;
    for (int i = (int)cycling_str.size() + 1; i < u_str.size();) {
        if (z_func[i] == cycling_str.size()) {
            if (f == -1)
                f = i;
            i += z_func[i];
        }
        else if (f != -1) {
            if (z_func[i] + i != u_str.size())
                return false;
            i += z_func[i];
        }
        else
            ++i;
    }

    if (f == -1 || f - cycling_str.size() - 1 > cycling_str.size())
        return false;

    for (int i = f - 1, j = (int)cycling_str.size() - 1; i > cycling_str.size(); --j) {
        if (cycling_str[j] != u_str[i])
            return false;
        --i;
    }

    return true;
}

